var cron = require("node-cron");
const clientService = require("./services/client.service");
var cron = require("node-cron");
var moment = require("moment");
const axios = require("axios");

function getDates(day) {
  const dt = new Date();
  const currentWeekDay = dt.getDay();
  const lessDays = currentWeekDay == 0 ? 4 : currentWeekDay - 1;
  const wkStart = new Date(new Date(dt).setDate(dt.getDate() - lessDays));
  const wkEnd = new Date(new Date(wkStart).setDate(wkStart.getDate() + day));
  const dt2 = new Date(wkEnd);
  const currentWeekDay2 = dt2.getDay();
  const lessDays2 = currentWeekDay2 == 0 ? 4 : currentWeekDay2 - 1;
  const wkStart2 = new Date(new Date(dt2).setDate(dt2.getDate() - lessDays2));
  const start2 = moment(
    new Date(new Date(dt2).setDate(dt2.getDate() - lessDays2))
  ).format("YYYY-MM-DD");
  const end2 = moment(
    new Date(new Date(wkStart2).setDate(wkStart2.getDate() + 4))
  ).format("YYYY-MM-DD");
  return { start: start2, end: end2 };
}
const req = {
  query: {
    sceduling: true,
  },
};
cron.schedule("* * * * *", () => {
  // console.log("running a task every minute");
  // console.log("week ",start2,end2)

  // sendMessageThroughSMS('7414940782')
  // sendMessageThroughWhatsApp('7414940782')
  // updateStatus()
  // getClient();
});
const sendMessage = cron.schedule("0 15 * * 4,5", () => {
  console.log("running a task every minute");
  // getClient();
});

///update status inactive if client did not pay before sunday 6 pm
const updateStatusInactive = cron.schedule("0 18 * * 7", () => {
  updateStatus();
});

async function updateStatus() {
  const dates = await getDates(63);
  const info = {
    paidAmount: 0,
    fromDate: dates.start,
    toDate: dates.end,
    paymentStatus: "inactive",
  };
  const data = await clientService.updateStatusInactive(
    dates.start,
    dates.end,
    info
  );
  console.log("this is for update status", data);
}

async function getClient() {
  const data = await clientService.getClient(req);
  const dates = getDates(7);
  console.log("date ==>", dates.start, dates.end);
  for (let info of data) {
    let flag = false;
    let flag2 = true;

    for (let week of info.weeklyPayment) {
      if (
        dates.start == moment(week.fromDate).format("YYYY-MM-DD") &&
        moment(dates.end).format("YYYY-MM-DD") ==
          moment(week.toDate).format("YYYY-MM-DD")
      ) {

        if (week.paymentStatus != "active") {
          flag = true;
          // console.log("not active",info.fullName);
        } else {
          flag2 = false;
        }
      } else {
        // console.log("not match ",info.fullName);
        flag = true;
      }
    }

    if (flag && flag2) {
      console.log("++++++++flag notifi  send notifications", info.fullName);
    }
  }
}

const sendMessageUsing360 = () => {
  // Replace with your actual credentials
  const CLIENT_ID = "your-client-id";
  const CLIENT_SECRET = "your-client-secret";
  const getToken = async () => {
    const response = await axios.post(
      "https://api-sandbox.360dialog.io/oauth/token",
      {
        grant_type: "client_credentials",
        client_id: CLIENT_ID,
        client_secret: CLIENT_SECRET,
      }
    );
    return response.data.access_token;
  };
  const sendTextMessage = async (accessToken, phoneNumber, message) => {
    const response = await axios.post(
      "https://api-sandbox.360dialog.io/v1/messages",
      {
        to: phoneNumber,
        type: "text",
        text: message,
      },
      {
        headers: {
          Authorization: `Bearer ${accessToken}`,
        },
      }
    );
    return response.data;
  };
  // Usage example
  (async () => {
    const accessToken = await getToken();
    const response = await sendTextMessage(
      accessToken,
      "1234567890",
      "Hello from Node.js!"
    );
    console.log(response);
  })();
};

sendMessageThroughWhatsApp = (to) => {
  try {
    console.log("called", to);
    const client = require("twilio")(
      "AC4c297734adfa0ad24c3c71da9ad9ab70",
      "62cbaa163b9033ddfd82fbe5221b719d"
    );
    client.messages
      .create({
        body: "hello whatsapp messages from yogesh pawar",
        from: "whatsapp:+14155238886", // process.env.TWILIO_PHONE_NUMBER,
        to: "whatsapp:+917414940782",
      })
      .then((message) => {
        console.log("message is==>", message.sid);
        // resp.send(message)
      });
  } catch (error) {
    console.log("error", error);
    // resp.status(401).send(error.message)
  }
};

//for twilio sms send
require("dotenv").config();
const accountSid = "AC591a36ef777a0f778a9b03d0ce6b7607"; // process.env.TWILIO_ACCOUNT_SID;
const authToken = "6ec650981af72b3ef53005da6848dcd0"; //process.env.TWILIO_AUTH_TOKEN;

sendMessageThroughSMS = (to) => {
  console.log("method called", authToken, accountSid);
  console.log("receiver number", to);
  try {
    const client = require("twilio")(accountSid, authToken);
    client.messages
      .create({
        body: "yogesh pawar send message using twilio ,please paid your amount before sunday",
        from: "+14305407730", // process.env.TWILIO_PHONE_NUMBER,
        to: "+917414940782", //9225108952
      })
      .then((message) => {
        console.log(message.sid);
        // resp.send(message)
      });
  } catch (error) {
    console.log("error==>", error);
    // resp.status(401).send(error.message)
  }
};

module.exports = { sendMessage, updateStatusInactive };
