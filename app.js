const express=require('express')
const http=require('http');
const dbConnection=require('./db.config');
const router=require('./routes/index');
const config=require('./config');
const mongoose = require('mongoose');
const bodyParser = require('body-parser'); 
const {sendMessage,updateStatusInactive}=require('./scheduling')
const app = express();
var server = http.createServer(app);
const cors = require('cors');
app.use(cors());
app.options('*', cors());


app.use(bodyParser.urlencoded({ extended: false }));
    app.use(bodyParser.json());

    app.use('/',router);

server.listen(config.PORT, ()=>{
   console.log(`Application Listening on Port ${config.PORT}`);
});

dbConnection.dbConnect()
