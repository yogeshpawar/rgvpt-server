const mongoose = require("mongoose");
var timestamps = require("mongoose-timestamp");

var ChildrenSchema = mongoose.Schema({
  clientId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Client",
    required: true,
  },
  childName: { type: String },
  schoolName: { type: String },
  dateOfBirth: { type: String },
  costType: { type: String },
});
ChildrenSchema.plugin(timestamps);

module.exports = mongoose.model("children", ChildrenSchema);
