const mongoose = require("mongoose");
var timestamps = require("mongoose-timestamp");

var InvoiceSchema = mongoose.Schema({
  clientId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Client",
    required: true,
  },
  paymentMode: {
    type: String,
    required: function () {
      return this.paymentMode != "" ? true : false;
    },
  },

  collectPaymentWeeks: [
    {
      fromDate: { type: Date },
      toDate: { type: Date },
      paidAmount: { type: Number },
      paymentStatus: { type: String, default: "active" },
    },
  ],
  totalPayment: { type: Number },
  subTotal: { type: Number },
  discountCost: { type: Number, default: 0 },
  extraCost: { type: Number, default: 0 },
  reasonForExtraCost: {
    type: String,
    required: function () {
      return this.reasonForExtraCost != "" ? true : false;
    },
  },
});
InvoiceSchema.plugin(timestamps);

module.exports = mongoose.model("invoice", InvoiceSchema);
