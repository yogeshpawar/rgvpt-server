const mongoose = require("mongoose");
const adminSchema = mongoose.Schema({
  full_name: String,
  mobile_no: String,
  password: String,
  email_id: String,
  user_role: String,
});
module.exports = mongoose.model("Admin", adminSchema);
