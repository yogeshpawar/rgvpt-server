const mongoose=require('mongoose');
var moment = require("moment");
var timestamps = require('mongoose-timestamp');

let dt = new Date()
let currentWeekDay = dt.getDay();
let lessDays = currentWeekDay == 0 ? 4 : currentWeekDay-1
let wkStart =new Date(new Date(dt).setDate(dt.getDate()- lessDays));
 let start=  moment(new Date(new Date(dt).setDate(dt.getDate()- lessDays))).format('YYYY-MM-DD')
 let end =moment( new Date(new Date(wkStart).setDate(wkStart.getDate()+4))).format('YYYY-MM-DD');

var clientSchema = mongoose.Schema({
    adminId : { type: mongoose.Schema.Types.ObjectId, ref: 'Client',required:true},
    clientId : {type: String},
    fullName : {type: String},
    primaryPhoneNumber:{type: String},
    secondaryPhoneNumber:{type: String},
profileStatus:{type:String,default:'unArchive'},
    relationship:{type: String},
    weeklyPayment:[{fromDate:{type:Date,default:start}, toDate:{type:Date,default:end} ,paymentStatus:{type:String,default:'inactive'},paidAmount:{type:Number,default:0} }]
});
clientSchema.plugin(timestamps);
module.exports = mongoose.model('Client', clientSchema);
