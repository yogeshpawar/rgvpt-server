const mongoose = require("mongoose");
const costSchema = mongoose.Schema({
  adminId: { type: mongoose.Schema.Types.ObjectId, ref: 'cost',required:true},
  costType: String,
  costRate: Number,
});
module.exports = mongoose.model("cost", costSchema);
