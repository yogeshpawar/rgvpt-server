const mongoose = require("mongoose");
const costSchema = mongoose.Schema({
  schoolName: String,
  adminId: { type: mongoose.Schema.Types.ObjectId, ref: 'school',required:true},
});
module.exports = mongoose.model("school", costSchema);
