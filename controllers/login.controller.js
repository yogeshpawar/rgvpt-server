const dbConnection = require("../db.config");
const Admin = require("../models/admin");
const jwt = require("jsonwebtoken");
const config = require("../config");
module.exports = {
  // Login User API
  loginUser: function (req, res) {
    try {
      Admin.find({ email_id: req.body?.email, password: req.body?.password })
        .exec()
        .then((admin) => {
          if (admin.length < 1) {
            return res
              .status(500)
              .json({ message: "Email and password not correct" });
          } else {
            const token = jwt.sign(
              {
                adminId: admin[0]?.admin_id,
                name: admin[0]?.full_name,
                email: admin[0]?.email_id,
                userRole: admin[0]?.user_role,
                mobileNo: admin[0]?.mobile_no,
              },
              config.AdminSecretKey,
              { expiresIn: "24h" }
            );

            return res.status(200).json({ data: admin[0], token: token });
          }
        })
        .catch((err) => {
          res.status(500).json(err.message);
        });
    } catch (error) {
      res.status(500).send(error.message);
    }
  },
  //Email Check API
  emailChecked: (req, resp) => {
    try {
      Admin.find({ email_id: req.body?.email })
        .exec()
        .then((admin) => {
          if (admin.length < 1) {
            return resp.status(400).send(false);
          } else {
            return resp.status(200).send(true);
          }
        })
        .catch((err) => {
          res.status(400).json(err.message);
        });
    } catch (error) {
      console.log("error==>", error.message);
      res.status(400).send(error.message);
    }
  },
  //Update Admin Password OR Reset Password
  updateAdminPassword: (req, resp) => {
    try {
      console.log("my email and password", req.body);
      Admin.findOneAndUpdate(
        { email_id: req.body?.email },
        {
          $set: {
            password: req.body?.password,
          },
        }
      )
        .then((result) => {
          resp
            .status(200)
            .json({ message: "Password Updated Successfully", data: result });
        })
        .catch((err) => {
          resp.status(500).json(err.message);
        });
    } catch (error) {
      resp.status(400).send(error.message);
    }
  },
};
