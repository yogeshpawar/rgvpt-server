const adminService = require("../services/admin.service");
module.exports = {
  getAdmin: async function (req, res) {
    try {
      const data = await adminService.getAdmin();
      res.send({ data: data });
    } catch (error) {
      res.status(500).send(error.message);
    }
  },
  postAdmin: async function (req, res) {
    try {
      const data = await adminService.postAdmin(req.body);
      res.status(200).send({ data: data });
    } catch (error) {
      res.status(500).send(error.message);
    }
  },
  postmessage: async function (req, resp) {
    console.log("my request", req);
    console.log("my response", resp);
  },
  postCost: async function (req, res) {
    try {
      const data = await adminService.postCost(req.body);
      res.status(200).send({ data: data });
    } catch (error) {
      res.status(500).send(error.message);
    }
  },
  getCost: async function (req, res) {
    try {
      const data = await adminService.getCost(req.params?.adminId);
      res.status(200).send({ data: data });
    } catch (error) {
      res.status(500).send(error.message);
    }
  },

  updateCost: async function (req, res) {
    try {
      const data = await adminService.updateCost(req);
      res.status(200).send({ data: data });
    } catch (error) {
      res.status(500).send(error.message);
    }
  },

  postSchool: async function (req, res) {
    try {
      const data = await adminService.postSchool(req.body);
      res.status(200).send({ data: data });
    } catch (error) {
      res.status(500).send(error.message);
    }
  },

  getSchool: async function (req, res) {
    try {
      const data = await adminService.getSchool(req.params?.adminId);
      res.status(200).send({ data: data });
    } catch (error) {
      res.status(500).send(error.message);
    }
  },

  updateSchool: async function (req, res) {
    try {
      const data = await adminService.updateSchool(req);
      res.status(200).send({ data: data });
    } catch (error) {
      res.status(500).send(error.message);
    }
  },
  
  deleteSchool: async function (req, res) {
    try {
      const data = await adminService.deleteSchool(req);
      res.status(200).send({ data: data });
    } catch (error) {
      res.status(500).send(error.message);
    }
  }
};
