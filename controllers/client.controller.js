const clientService = require("../services/client.service");
const adminService = require("../services/admin.service");
var moment = require("moment");
module.exports = {
  getClient: async function (req, res) {
    try {
      const data = await clientService.getClient(req);
      const newData = await clientService.mappedClientData(data[0]?.data,req.params?.adminId);
      res.status(200).send({
        data: newData ?? [],
        total: data[0]?.metadata[0]?.total ?? 0,
        page: data[0]?.metadata[0]?.page,
      });
    } catch (error) {
      res.status(500).send(error.message);
    }
  },
  getAllClients: async function (req, resp) {
    try {
      const data = await clientService.getAllClients(req);
      const newData = await clientService.mappedClientData(data[0]?.data,req.params?.adminId);
      resp.status(200).send({
        data: newData ?? [],
        total: data[0]?.metadata[0]?.total ?? 0,
        page: data[0]?.metadata[0]?.page,
      });
    } catch (error) {
      resp.status(500).send(error.message);
    }
  },

  updateStatusInactive: async function () {
    const data = await clientService.updateStatusInactive();
  },
  getWeeklyPayment: async function (req, resp) {
    try {
      const data = await clientService.getWeeklyPayment(req);
      resp.status(200).send(data);
    } catch (error) {
      resp.status(500).send(error.message);
    }
  },
  countClients: async function (req, resp) {
    try {
      const data = await clientService.getClientCount(req);
      const newData = {
        activeClients: data[0]?.activeClients[0]?.mycount ?? 0,
        inactiveClients: data[0]?.inactiveClients[0]?.mycount ?? 0,
        totalClient: data[0]?.totalClients[0]?.mycount ?? 0,
        archiveClients: data[0]?.archiveClients[0]?.mycount ?? 0,
        unArchiveClients:data[0]?.unArchiveClients[0]?.mycount ?? 0,
      };
      resp.status(200).send({ data: newData });
    } catch (error) {
      resp.status(500).send(error.message);
    }
  },

  getChildrens: async function (req, resp) {
    try {
      console.log("request ==>",req.params?.adminId)
      const data = await clientService.getChildren(req);

      resp.status(200).send({ data: data });
    } catch (error) {
      console.log("error=>", error.message);
      resp.status(500).send(error.message);
    }
  },
  postClient: async function (req, res) {
    const isClientExist= await clientService.isClientExist(req.body?.primaryPhoneNumber)

    if (isClientExist.length>0) {
      return res.status(403).json({ message:"Client is Already Exist"});
    }
    let weeklyPayment = [];
    let week = 7;
    const dt = new Date();
    const currentWeekDay = dt.getDay();
    const lessDays = currentWeekDay == 0 ? 4 : currentWeekDay - 1;
    const fromDate = new Date(new Date(dt).setDate(dt.getDate() - lessDays));
    const toDate = new Date(new Date(fromDate).setDate(fromDate.getDate() + 4));
    weeklyPayment.push({
      fromDate: fromDate,
      toDate: toDate,
      paymentStatus: "inactive",
      paidAmount: 0,
    });

    for (let i = 0; i < 8; i++) {
      const dt = new Date();
      const currentWeekDay = dt.getDay();
      const lessDays = currentWeekDay == 0 ? 4 : currentWeekDay - 1;
      let start = new Date(new Date(dt).setDate(dt.getDate() - lessDays));
      const end = new Date(new Date(start).setDate(start.getDate() + week));
      //next week
      const dt2 = new Date(end);
      const currentWeekDay2 = dt2.getDay();
      const lessDays2 = currentWeekDay2 == 0 ? 4 : currentWeekDay2 - 1;
      let start2 = new Date(new Date(dt2).setDate(dt2.getDate() - lessDays2));
      const end2 = new Date(new Date(start2).setDate(start2.getDate() + 4));
      weeklyPayment.push({
        fromDate: start2,
        toDate: end2,
        paymentStatus: "inactive",
        paidAmount: 0,
      });
      week += 7;
    }
    const client = {
      clientId: req.body?.clientId,
      adminId: req.body?.adminId,
      fullName: req.body?.fullName,
      primaryPhoneNumber: req.body?.primaryPhoneNumber,
      secondaryPhoneNumber: req.body?.secondaryPhoneNumber,
      relationship: req.body?.relationship,
      weeklyPayment: [...weeklyPayment],
    };

    try {
      const data = await clientService.postClient(client);
      if (data._id) {
        let data1 = await req.body?.childrens.forEach(async (curr) => {
          await clientService.postChildren({ ...curr, clientId: data._id });
        });
      }
      res.status(200).send({ data: data });
    } catch (error) {
      console.log("error===>", error.message);
      res.status(500).send(error.message);
    }
  },
  updateClientStatus: async function (req, res) {
    try {
      const data = await clientService.updateClientStatus(req);
      res.status(200).send({ data: data });
    } catch (error) {
      console.log("error", error.message);
      res.status(500).send(error.message);
    }
  },
  updateClient: async function (req, res) {
    try {
      const data = await clientService.updateClient(req);
      if (data) {
        let data1 = await req.body.childrens.forEach(async (curr) => {
          if (curr._id) {
             curr.delete ?  await clientService.deleteChildren(curr._id) :  await clientService.updateChildren(req.params.id, curr);

          } else {
            await clientService.postChildren({
              ...curr,
              clientId: req.params?.id,
            });
          }
        });
        res.status(200).send({ data: data });
      }
    } catch (err) {
      res.status(500).send(err.message);
    }
  },
  getLatestInvoiceWeek: async function (req, resp) {
    try {
      const data = await clientService.getLatestInvoiceWeek(req);
      resp.status(200).send(data);
    } catch (error) {
      console.log("error", error);
      resp.status(500).send(error.message);
    }
  },
};
