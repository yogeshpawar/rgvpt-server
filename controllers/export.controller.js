const ExportService = require("../services/export-service");
const clientService = require("../services/client.service");
const exportService = new ExportService();

module.exports = {
  exportClient: async function (req, res) {
    let status = req.query.status;
    try {
      let allClients = await clientService.getClient(req);
      let processClients = [];
      for (const client of allClients[0].data) {
        processClients.push({
          id: client._id.toHexString(),
          "full Name": client?.fullName,
          "primary Phone Number:": client?.primaryPhoneNumber,
          "Secondary Phone Number": client?.secondaryPhoneNumber,
          "Total Amount": client?.totalAmount,
          "payment Status": client?.weeklyPayment?.paymentStatus,
          relationship: client?.relationship,
          "Number Of Childrens": client?.childrens.length,
        });
      }
      let data = {
        Users: processClients,
      };
      let buf = await exportService.createWorkBook(data);
      console.log("buf", buf);
      var fileName =
        "export-" + req?.query?.fromDate + "--" + req?.query?.toDate + ".xlsx";
      res.setHeader(
        "Content-Type",
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
      );
      res.setHeader("Content-Disposition", "attachment; filename=" + fileName);
      res.status(200).send(buf);
    } catch (error) {
      console.log("catch error", error);
      res.status(500).send(error.message);
    }
  },
};
