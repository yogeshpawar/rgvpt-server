const Client = require("../models/client");
const Invoice = require("../models/invoice");
const Children = require("../models/children");
var moment = require("moment");
const ObjectId = require("mongoose").Types.ObjectId;
const admin = require("../models/admin");
module.exports = {
    postClient: async (data) => {
        const client = new Client(data);
        return await client.save();
    },
    updateStatusInactive: async (fromDate, toDate, data) => {
        var clientInfo = await Client.find({ profileStatus: "unArchive" });
        if (clientInfo) {
            for (let client of clientInfo) {
                let flag = true;
                for (let week of client.weeklyPayment) {
                    let from = moment(week.fromDate).format("YYYY-MM-DD");
                    let to = moment(week.toDate).format("YYYY-MM-DD");
                    if (from == fromDate && to == toDate) {
                        flag = false;
                    }
                }
                if (flag) {
                    let resultData = await Client.findOneAndUpdate(
                        { _id: client._id },
                        { $push: { weeklyPayment: data } }
                    ).then((result) => {
                        console.log(result);
                    });
                }
            }
        }
    },

    getWeeklyPayment: async (data) => {
        return await Client.aggregate([
            {
                $match: {
                    _id: ObjectId(data.params?.id),
                },
            },
            {
                $lookup: {
                    from: "childrens",
                    foreignField: "clientId",
                    localField: "_id",
                    as: "childrens",
                },
            },
        ]);
    },
    postChildren: async (data) => {
        const child = new Children(data);
        return await child.save();
    },

    getChildren: async (req) => {
        let adminId = req.params?.adminId;
        let temp = await Client.aggregate([
            {
                $match: {
                    adminId: ObjectId(adminId),
                    profileStatus:{$ne:'archive'}
                },
            },
            {
                $group: {
                    _id: "$_id",
                },
            },
        ]);
        return await Children.find({ clientId: { $in: temp } });

        // return await Children.find();
    },

    getClientsCount: async (req) => {
        let adminId = req.params?.adminId;
        let fromDate = moment(req.query?.fromDate).format("YYYY-MM-DD");
        let toDate = moment(req.query?.toDate).format("YYYY-MM-DD");
        const res = await Client.aggregate([
            { $match: { adminId: ObjectId(adminId) } },
            {
                $facet: {
                    inactiveClients: [
                        { $match: { profileStatus: "unArchive" } },
                        { $unwind: "$weeklyPayment" },
                        {
                            $match: {
                                $and: [
                                    {
                                        "weeklyPayment.fromDate": {
                                            $gte: new Date(
                                                fromDate + "T00:00:00.000+00:00"
                                            ),
                                        },
                                    },
                                    {
                                        "weeklyPayment.toDate": {
                                            $lte: new Date(
                                                toDate + "T23:59:59.999+00:00"
                                            ),
                                        },
                                    },
                                    {
                                        "weeklyPayment.paymentStatus":
                                            "inactive",
                                    },
                                ],
                            },
                        },
                        { $count: "mycount" },
                    ],
                    activeClients: [
                        { $match: { profileStatus: "unArchive" } },
                        { $unwind: "$weeklyPayment" },
                        {
                            $match: {
                                $and: [
                                    {
                                        "weeklyPayment.fromDate": {
                                            $gte: new Date(
                                                fromDate + "T00:00:00.000+00:00"
                                            ),
                                        },
                                    },
                                    {
                                        "weeklyPayment.toDate": {
                                            $lte: new Date(
                                                toDate + "T23:59:59.999+00:00"
                                            ),
                                        },
                                    },
                                    { "weeklyPayment.paymentStatus": "active" },
                                ],
                            },
                        },
                        { $count: "mycount" },
                    ],
                    archiveClients: [
                        {
                            $match: { profileStatus: "archive" },
                        },
                        { $count: "mycount" },
                    ],
                    totalClients: [
                        {
                            $match: {},
                        },
                        { $count: "mycount" },
                    ],
                    unArchiveClients: [
                        {
                            $match: { profileStatus: "unArchive" },
                        },
                        { $count: "mycount" },
                    ],
                },
            },
        ]);
        return res;
    },

    getClient: async (req) => {
        let status = req.query?.status;
        let adminId = req.params?.adminId;
        let search = req?.query?.search ? req?.query?.search : "";
        let regexPattern = new RegExp(".*" + search + ".*", "i");
        const fromDate = req.query?.fromDate;
        const toDate = req.query?.toDate;
        let page =
            req?.query?.page == "undefined"
                ? "undefined"
                : parseInt(req?.query?.page - 1);
        scheduling = req?.query?.sceduling;
        if (scheduling) {
            return await Client.aggregate([
                { $match: { profileStatus: "unArchive" } },
                {
                    $lookup: {
                        from: "childrens",
                        foreignField: "clientId",
                        localField: "_id",
                        as: "childrens",
                    },
                },
            ]);
        } else if (page != "undefined" && status == "undefined") {
            console.log("page != undefined &&status ==undefined");
            return await Client.aggregate([
                {
                    $match: {
                        adminId: ObjectId(adminId),
                        profileStatus: "unArchive",
                        $or: [
                            { clientId: { $regex: regexPattern } },
                            { fullName: { $regex: regexPattern } },
                            { primaryPhoneNumber: { $regex: regexPattern } },
                        ],
                    },
                },
                { $unwind: "$weeklyPayment" },
                {
                    $match: {
                        $and: [
                            {
                                "weeklyPayment.fromDate": {
                                    $gte: new Date(
                                        fromDate + "T00:00:00.000+00:00"
                                    ),
                                },
                            },
                            {
                                "weeklyPayment.toDate": {
                                    $lte: new Date(
                                        toDate + "T23:59:59.999+00:00"
                                    ),
                                },
                            },
                        ],
                    },
                },
                {
                    $lookup: {
                        from: "childrens",
                        foreignField: "clientId",
                        localField: "_id",
                        as: "childrens",
                    },
                },
                {
                    $facet: {
                        metadata: [
                            { $count: "total" },
                            { $addFields: { page: parseInt(page) } },
                        ],
                        data: [{ $skip: page * 100 }, { $limit: 100 }],
                    },
                },
            ]);
        } else if (page == "undefined" && status != "undefined") {
            console.log("page == undefined &&status != undefined");
            const data = await Client.aggregate([
                {
                    $match: {
                        adminId: ObjectId(adminId),
                        profileStatus: "unArchive",
                        $or: [
                            { clientId: { $regex: regexPattern } },
                            { fullName: { $regex: regexPattern } },
                            { primaryPhoneNumber: { $regex: regexPattern } },
                        ],
                    },
                },
                { $unwind: "$weeklyPayment" },
                {
                    $match: {
                        $and: [
                            {
                                "weeklyPayment.fromDate": {
                                    $gte: new Date(
                                        fromDate + "T00:00:00.000+00:00"
                                    ),
                                },
                            },
                            {
                                "weeklyPayment.toDate": {
                                    $lte: new Date(
                                        toDate + "T23:59:59.999+00:00"
                                    ),
                                },
                            },
                            { "weeklyPayment.paymentStatus": status },
                        ],
                    },
                },
                {
                    $lookup: {
                        from: "childrens",
                        foreignField: "clientId",
                        localField: "_id",
                        as: "childrens",
                    },
                },
                { $sort: { _id: -1 } },
                {
                    $facet: {
                        metadata: [{ $count: "total" }],
                        data: [{ $skip: 0 }],
                    },
                },
            ]);
            return data;
        } else if (page == "undefined" && status == "undefined") {
            console.log("page == undefined &status ==undefined ");
            return await Client.aggregate([
                {
                    $match: {
                        adminId: ObjectId(adminId),
                        profileStatus: "unArchive",
                        $or: [
                            { clientId: { $regex: regexPattern } },
                            { fullName: { $regex: regexPattern } },
                            { primaryPhoneNumber: { $regex: regexPattern } },
                        ],
                    },
                },
                { $unwind: "$weeklyPayment" },
                {
                    $match: {
                        $and: [
                            {
                                "weeklyPayment.fromDate": {
                                    $gte: new Date(
                                        fromDate + "T00:00:00.000+00:00"
                                    ),
                                },
                            },
                            {
                                "weeklyPayment.toDate": {
                                    $lte: new Date(
                                        toDate + "T23:59:59.999+00:00"
                                    ),
                                },
                            },
                        ],
                    },
                },
                {
                    $lookup: {
                        from: "childrens",
                        foreignField: "clientId",
                        localField: "_id",
                        as: "childrens",
                    },
                },
                { $sort: { _id: -1 } },
                {
                    $facet: {
                        metadata: [{ $count: "total" }],
                        data: [{ $skip: 0 }],
                    },
                },
            ]);
        } else {
            // this block is called when status is not undefined and page is not undefined
            console.log("else  block");
            let data = await Client.aggregate([
                {
                    $match: {
                        adminId: ObjectId(adminId),
                        profileStatus: "unArchive",
                        $or: [
                            { clientId: { $regex: regexPattern } },
                            { fullName: { $regex: regexPattern } },
                            { primaryPhoneNumber: { $regex: regexPattern } },
                        ],
                    },
                },
                { $unwind: "$weeklyPayment" },
                {
                    $match: {
                        $and: [
                            {
                                "weeklyPayment.fromDate": {
                                    $gte: new Date(
                                        fromDate + "T00:00:00.000+00:00"
                                    ),
                                },
                            },
                            {
                                "weeklyPayment.toDate": {
                                    $lte: new Date(
                                        toDate + "T23:59:59.999+00:00"
                                    ),
                                },
                            },
                            { "weeklyPayment.paymentStatus": status },
                        ],
                    },
                },
                {
                    $lookup: {
                        from: "childrens",
                        foreignField: "clientId",
                        localField: "_id",
                        as: "childrens",
                    },
                },
                {
                    $facet: {
                        metadata: [
                            { $count: "total" },
                            { $addFields: { page: parseInt(page) } },
                        ],
                        data: [{ $skip: page * 100 }, { $limit: 100 }],
                    },
                },
            ]);

            return data;
        }
    },

    isClientExist: async (phoneNumber) => {
        return await Client.aggregate([
            { $match: { primaryPhoneNumber: phoneNumber } },
        ]);
    },

    getAllClients: async (req) => {
        let profileStatus = req?.query?.profileStatus;
        let adminId = req?.params?.adminId;
        let search = req?.query?.search;
        let page = req?.query?.page - 1;
        let query = {};
        let regexPattern = new RegExp(".*" + search + ".*", "i");
        if (profileStatus != "undefined") {
            query = {
                $match: {
                    adminId: ObjectId(adminId),
                    profileStatus: profileStatus,
                    $or: [
                        { clientId: { $regex: regexPattern } },
                        { fullName: { $regex: regexPattern } },
                        { primaryPhoneNumber: { $regex: regexPattern } },
                    ],
                },
            };
            
        } else {
            query = {
                $match: {
                    adminId: ObjectId(adminId),
                    $or: [
                        { clientId: { $regex: regexPattern } },
                        { fullName: { $regex: regexPattern } },
                        { primaryPhoneNumber: { $regex: regexPattern } },
                    ],
                },
            };
        }
        return await Client.aggregate([
            query,
            {
                $lookup: {
                    from: "childrens",
                    foreignField: "clientId",
                    localField: "_id",
                    as: "childrens",
                },
            },
            {
                $facet: {
                    metadata: [
                        { $count: "total" },
                        { $addFields: { page: parseInt(page) } },
                    ],
                    data: [{ $skip: page * 100 }, { $limit: 100 }],
                },
            },
        ]);
    },

    updateClientStatus: async (data) => {
        return Client.findOneAndUpdate(
            { _id: data.params.id },
            { $set: { weeklyPayment: data.body?.weeklyPayment } }
        ).then((result) => {
            return result;
        });
    },

    postInvoice: async (data) => {
        let invoice = new Invoice(data);
        return await invoice.save();
    },

    getLatestInvoiceWeek: async (data) => {
        todayDate = moment(new Date()).format("YYYY-MM-DD");
        let temp = await Invoice.aggregate([
            { $match: { clientId: ObjectId(data?.params?.clientId) } },
            { $unwind: "$collectPaymentWeeks" },
        ]);

        return temp;
    },

    updateClient: async (data) => {
        return Client.findOneAndUpdate(
            { _id: data.params.id },
            {
                $set: {
                    fullName: data.body?.fullName,
                    primaryPhoneNumber: data.body?.primaryPhoneNumber,
                    secondaryPhoneNumber: data.body?.secondaryPhoneNumber,
                    relationship: data.body?.relationship,
                    profileStatus: data.body?.profileStatus,
                    clientId: data.body?.clientId,
                },
            }
        ).then((result) => {
            return result;
        });
    },
    updateChildren: async (clientId, data) => {
        return Children.findOneAndUpdate(
            { clientId: clientId, _id: data._id },
            {
                $set: {
                    childName: data.childName,
                    schoolName: data.schoolName,
                    dateOfBirth: data.dateOfBirth,
                    costType: data.costType,
                    payableAmount: data.payableAmount,
                },
            }
        ).then((result) => {
            return result;
        });
    },
    deleteChildren: async (id) => {
        return Children.deleteOne({ _id: id });
    },
};
