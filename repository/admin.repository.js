const Admin = require("../models/admin");
const Cost = require("../models/costType");
const School= require("../models/schools")
const ObjectId = require("mongoose").Types.ObjectId;

module.exports = {
  getAdmin: async () => {
    return await Admin.find();
  },
  postAdmin: async (data) => {
    const admin = new Admin(data);
    return await admin.save();
  },

  postCost: async (data) => {
    const cost = new Cost(data);
    return await cost.save();
  },

  getCost: async (adminId) => {
     return await Cost.aggregate([
      { $match: { adminId: ObjectId(adminId)} },
      { $sort: { _id: 1 } },
    ]);
  },

  updateCost: async (data) => {
    return await Cost.findOneAndUpdate(
      { _id: data.params.id },
      {
        $set: {
          costRate: data.body?.costRate,
        },
      }
    );
  },

  postSchool: async (data) => {
    const school = new School(data);
    return await school.save();
  },

  getSchool: async (adminId) => {
    // return await School.find();
    return await School.aggregate([
      { $match: { adminId: ObjectId(adminId)} },
      { $sort: { _id: 1 } },
    ]);
  },
  
  updateSchool: async (data) => {
    return await School.findOneAndUpdate(
      { _id: data.params.id },
      {
        $set: {
          schoolName: data.body?.schoolName,
        },
      }
    );
  },
  deleteSchool: async (data) => {
    return await School.findOneAndDelete(
      { _id: data.params.id },
    );
  },
};
