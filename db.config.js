const mongoose = require('mongoose');
const config=require('./config');

module.exports={
  dbConnect:()=>{
    mongoose.set('strictQuery', false);
    mongoose.connect(
      config.DB_URL,
      { useNewUrlParser: true, useUnifiedTopology: true },
      function (err, conn) {
        if (!err) {
          console.log('Database connected');
        } else {
         console.log("not Connected",err)
        }
      }
    );
  }
};