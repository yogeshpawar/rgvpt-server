const router = require("express").Router();

const exportController = require("../controllers/export.controller");

router.get("/exportClients/:adminId", exportController.exportClient);

module.exports = router;
