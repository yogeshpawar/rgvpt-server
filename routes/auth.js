const router = require("express").Router();
const LoginController = require("../controllers/login.controller");

router.post("/login", LoginController.loginUser);
router.post("/emailCheck", LoginController.emailChecked);
router.put("/updatePassword", LoginController.updateAdminPassword);

module.exports = router;
