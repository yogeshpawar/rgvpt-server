const router = require("express").Router();
const adminController = require("../controllers/admin.controller");
const authentication = require("../jwt.verify");

router.get("/getAdmin", adminController.getAdmin);
router.post("/postAdmin", adminController.postAdmin);
router.post("/webhook", adminController.postmessage);
router.post("/postCost", authentication, adminController.postCost);
router.get("/getCost/:adminId", authentication, adminController.getCost);
router.put("/updateCost/:id", authentication, adminController.updateCost);

router.get("/getSchools/:adminId", authentication, adminController.getSchool);
router.put("/updateSchool/:id", authentication, adminController.updateSchool);
router.post("/postSchool", authentication, adminController.postSchool);
router.delete("/deleteSchool/:id", authentication, adminController.deleteSchool);

module.exports = router;
