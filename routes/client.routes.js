const router = require("express").Router();
const clientController = require("../controllers/client.controller");

router.get("/getClient/:adminId", clientController.getClient);
router.get("/allClients/:adminId", clientController.getAllClients);
router.post("/postClient", clientController.postClient);
router.put("/update-client/:id", clientController.updateClient);
router.put("/update-client-status/:id", clientController.updateClientStatus);
router.get("/counts-clients", clientController.countClients);
router.get("/get-childrens/:adminId", clientController.getChildrens);
router.get("/clientsCount/:adminId", clientController.countClients);
router.get("/invoice/:clientId", clientController.getLatestInvoiceWeek);
router.get("/get-weeklyPayment/:id", clientController.getWeeklyPayment);

module.exports = router;
