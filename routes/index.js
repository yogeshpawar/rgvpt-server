const authentication = require("../jwt.verify");
const router = require("express").Router();

router.use("/auth", require("./auth"));
router.use("/admin", require("./admin.routes"));

router.use(function (req, res, next) {
  authentication(req, res, next);
});

router.use("/client", authentication, require("./client.routes"));
router.use("/export", authentication, require("./export.routes"));
module.exports = router;
