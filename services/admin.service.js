const admin = require("../models/admin");
const adminRepository = require("../repository/admin.repository");

module.exports = {
  getAdmin: async () => {
    return await adminRepository.getAdmin();
  },
  postAdmin: async (data) => {
    return await adminRepository.postAdmin(data);
  },
  postCost: async (data) => {
    return await adminRepository.postCost(data);
  },
  getCost: async (adminId) => {
    return await adminRepository.getCost(adminId);
  },
  updateCost: async (data) => {
    return await adminRepository.updateCost(data);
  },
  postSchool: async (data) => {
    return await adminRepository.postSchool(data);
  },
  getSchool: async (adminId) => {
    return await adminRepository.getSchool(adminId);
  },
  updateSchool: async (data) => {
    return await adminRepository.updateSchool(data);
  },
  deleteSchool: async (data) => {
    return await adminRepository.deleteSchool(data);
  },
};
