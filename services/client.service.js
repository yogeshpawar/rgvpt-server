const clientRepository = require("../repository/client.repository");
const adminService = require("../services/admin.service");

module.exports = {
  getClient: async (req) => {
    return await clientRepository.getClient(req);
  },
  getAllClients: async (req) => {
    return await clientRepository.getAllClients(req);
  },
  postClient: async (data) => {
    return await clientRepository.postClient(data);
  },
  updateStatusInactive: async (fromDate, toDate, data) => {
    return await clientRepository.updateStatusInactive(fromDate, toDate, data);
  },
  getWeeklyPayment: async (data) => {
    return await clientRepository.getWeeklyPayment(data);
  },
  postChildren: async (data) => {
    return await clientRepository.postChildren(data);
  },
  getChildren: async (req) => {
    return await clientRepository.getChildren(req);
  },
  isClientExist:async(req)=>{
    return await clientRepository.isClientExist(req)
  },
  updateClient: async (data) => {
    return await clientRepository.updateClient(data);
  },

  getClientCount: async (req) => {
    return await clientRepository.getClientsCount(req);
  },
  updateClientStatus: async (data) => {
    const result = await clientRepository.updateClientStatus(data);
    const invoiceResult = await clientRepository.postInvoice(
      data.body?.invoiceData
    );
    return result;
  },
  getLatestInvoiceWeek: async (data) => {
    return await clientRepository.getLatestInvoiceWeek(data);
  },
  deleteChildren: async (id) => {
    return await clientRepository.deleteChildren(id);
  },
  updateChildren: async (id, data) => {
    return await clientRepository.updateChildren(id, data);
  },
  mappedClientData: async function (data,adminId) {
    const costs = await adminService.getCost(adminId);
    CostRates = [];
    for (let info of costs) {
      CostRates = {
        ...CostRates,
        [info.costType]: info.costRate,
      };
    }
    const newData = data.map((client) => {
      //calculate totalAmount based on childrens cost types

      let Total = client.childrens.reduce(
        (accum, curr) => accum + CostRates[curr.costType],
        0
      );
      
      //calculate payable amount for each child
      let newChildrens = client?.childrens.map((child) => {
        return { ...child, payableAmount: CostRates[child.costType] };
      });
      return {
        ...client,
        totalAmount: Total,
        childrens: newChildrens,
      };
    });
    return newData;
  },
};
