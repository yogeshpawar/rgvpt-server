const XLSX = require("xlsx");

class ExportService {
  async createWorkBook(data, bookType = "xlsx") {
    try {
      let workBook = XLSX.utils.book_new();
      Object.keys(data).forEach((k) => data[k].length == 0 && delete data[k]);
      const sheets = Object.entries(data);
      for (const [workSheetName, workSheetData] of sheets) {
        let ws_data = [];
        let dataHeaderEntries = Object.entries(workSheetData[0]);
        let headerRow = [];
        for (const [fieldName, fieldData] of dataHeaderEntries) {
          headerRow.push(fieldName);
        }
        ws_data.push(headerRow);
        for (var i = 0; i < workSheetData.length; i++) {
          const dataEntries = Object.entries(workSheetData[i]);
          let dataRow = [];
          for (const [fieldName, fieldData] of dataEntries) {
            dataRow.push(fieldData);
          }
          ws_data.push(dataRow);
        }
        let workSheet = XLSX.utils.aoa_to_sheet(ws_data);
        XLSX.utils.book_append_sheet(workBook, workSheet, workSheetName);
      }
      return XLSX.write(workBook, { type: "buffer", bookType: bookType });
    } catch (err) {
      throw err;
    }
  }
}

module.exports = ExportService;
