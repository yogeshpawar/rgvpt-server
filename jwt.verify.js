const jwt = require("jsonwebtoken");
const config = require("./config");
const authentication = async (req, res, next) => {
  // console.log("local storage",localStorage.getItem('token'))
  const authHeader = req.headers.authorization;
  if (!authHeader) {

    return next({ status: 401, message: "Unauthorized" });
  }
  const token = authHeader.split(" ")[1];
  if (!token || authHeader.split(" ")[0] !== "Bearer") {

    return next({ status: 401, message: "Unauthorized" });
  }
  try {
    jwt.verify(token, config.AdminSecretKey, (error, decode) => {
      if (error) {
        if (error?.name == "TokenExpiredError") {
          console.log("my error is jwt", error);
          throw error;
        }
      }
    });
    next();
  } catch (err) {
    return next({ status: 401, message: "Unauthorized" });
  }
};

module.exports = authentication;
